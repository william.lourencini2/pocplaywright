<img src="./support/img/playwright.webp" alt="c"/></img>

# POC Playwright + Typescript

## Executando o Projeto:

**1. Abre o projeto com VScode e execute o comando:**

> npm install

**2. Para executar o projeto execute o comando:**

> npm run test

**3. Enviando os artefatos para o Allure:**

> ./allure.sh

**4. Scan com Sonarqube:**

> ./sonar.sh
