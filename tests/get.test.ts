import { test, expect } from '@playwright/test';

let id;

test.beforeAll(async ({ request }) => {
  const resp = await request.get('/posts');
  const respjson = await resp.json()
  id = await respjson[0]['id']
})

test.describe('GET', () => {
  test('Get all Posts', async ({ request }) => {
    const resp = await request.get('/posts');
    const respjson = await resp.json()
    //console.log(respjson)
    expect(resp.status()).toEqual(200)
  })
})

test.describe('GET', () => {
  test('GET Posts By ID', async ({ request }) => {
    const getById = await request.get(`/posts/${id}`)
    let respjson = await getById.json()
    expect(getById.status()).toEqual(200)
    expect(respjson['id']).toEqual(id)
    // console.log(respjson['id'])
    // console.log(respjson)
  })
})