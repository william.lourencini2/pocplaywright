import { test, expect } from '@playwright/test';
import { body } from '../support/mock/body.json'

let id;

test.beforeAll(async ({ request }) => {
  const resp = await request.get('/posts');
  const respjson = await resp.json()
  id = await respjson[0]['id']
})

test.describe('PUT', async () => {

  test('PUT By ID', async ({ request }) => {
    const respPut = await request.put(`/posts/${id}`, {
      data: {
        body,
      }
    });

    const respToJson = await respPut.json()
    expect(respPut.status()).toEqual(200)
    expect(respToJson['body']['title']).toEqual('Meu teste atualizado')
    expect(respToJson['body']['body']).toEqual('bar atualizado')
    expect(respToJson['body']['userId']).toEqual(1)
  });
})