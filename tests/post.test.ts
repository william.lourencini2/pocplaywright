import { test, expect } from '@playwright/test';

test.describe('POST', async () => {

  test('POST By ID', async ({ request }) => {
    const respPost = await request.post(`/posts/`, {
      data: {
        body: {
          title: 'Meu teste',
          body: 'bar',
          userId: 1,
        },
      }
    });

    const respToJson = await respPost.json()
    expect(respPost.status()).toEqual(201)
    expect(respToJson['body']['title']).toEqual('Meu teste')
    expect(respToJson['body']['body']).toEqual('bar')
    expect(respToJson['body']['userId']).toEqual(1)
  });
})