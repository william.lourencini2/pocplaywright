import { test, expect } from '@playwright/test';

let id;

test.beforeAll(async ({ request }) => {
  const resp = await request.get('/posts');
  const respjson = await resp.json()
  id = await respjson[0]['id']
})

test.describe('DELETE', () => {
  test('DELETE Posts By ID', async ({ request }) => {
    const deleteById = await request.delete(`/posts/${id}`)
    let respjson = await deleteById.json()
    expect(deleteById.status()).toEqual(200)
    expect(respjson).toEqual({})
  })
})